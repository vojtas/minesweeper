# Minesweeper Generator #

Implementation of minesweeper task

### Build ###

* dotnet restore
* dotnet build -c Release -o App

Or you can open it in VS / VS Code

### Run ###

In App takes 3 arguments - width height number_of_mines.
./App.exe 15 15 10

### Implementation ###

I have implemented 2 different methods for mine generation.

* GenerateField
Ramdomly chooses both dimensions in matrix and if it's possible to put mine there decreases mines counter. 

* GenerateField2
Iterates through ever cell of matrix and decides to put mine there based on dynamically changing probability.

Clue generation is done every time a mine is placed by incrementing MinesInArea counter for all 8 fields in range if they are still neutral fields.

You can initialize MineSweeper with matrix too (used for unit tests).