﻿using System;
using System.Text;

namespace MineSweeper.Core
{
    public class MineSweeper
    {
        private readonly int Width;
        private readonly int Height;
        private readonly int Mines;

        private Cell[,] Field;

        public MineSweeper(int width, int height, int mines)
        {
            Width = width;
            Height = height;
            Mines = mines;
            Field = new Cell[width, height];
        }

        public MineSweeper(bool[,] matrix)
        {
            Width = matrix.GetLength(0);
            Height = matrix.GetLength(1);
            Field = new Cell[Width, Width];
            for (int x = 0; x < Width; x++) {
                for (int y = 0; y < Height; y++) {                    
                    if (matrix[x, y]) {
                        Field[x, y] = new Cell(CellType.Mine);
                        UpdateCellNeighbors(x, y);
                    } else if (Field[x, y] == null) {
                        Field[x, y] = new Cell();
                    }
                }
            }
        }

        public void GenerateField()
        {
            InitField();
            var generator = new Random();
            var remainingMines = Mines;

            while (remainingMines > 0)
            {
                var i = generator.Next(Width);
                var j = generator.Next(Height);

                if (Field[i, j].Type == CellType.Mine)
                    continue;

                Field[i, j].Type = CellType.Mine;
                UpdateCellNeighbors(i, j);
                remainingMines--;
            }
        }

        public void GenerateField2()
        {
            InitField();
            var generator = new Random();
            var remainingCells = Width * Height;
            var remainingMines = Mines;

            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Height; j++)
                {
                    if (generator.Next(remainingCells - remainingMines) < remainingMines)
                    {
                        Field[i, j].Type = CellType.Mine;
                        UpdateCellNeighbors(i, j);
                        remainingMines--;
                    }
                    remainingCells--;
                }
            }
        }

        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Height; j++)
                {
                    stringBuilder.Append("|");
                    stringBuilder.Append(
                        Field[i, j].Type == CellType.Mine ? "X" :
                        Field[i, j].MinesInArea == 0 ? " " : Field[i, j].MinesInArea.ToString());

                }
                stringBuilder.AppendLine("|");
            }
            return stringBuilder.ToString();
        }

        private void InitField()
        {
            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Height; j++)
                {
                    Field[i, j] = new Cell();
                }
            }
        }

        private void UpdateCellNeighbors(int i, int j)
        {
            for (int x = -1; x <= 1; x++) {
                for (var y = -1; y <= 1; y++) {
                    if (x == 0 && y == 0) {
                        continue;
                    }
                    var x_coordinate = i + x;
                    var y_coordinate = j + y;
                    if (DimensionsInBounds(x_coordinate, y_coordinate)) {
                        if (Field[x_coordinate, y_coordinate] == null) {
                            Field[x_coordinate, y_coordinate] = new Cell();
                        }
                        Field[x_coordinate, y_coordinate].AddMineInArea();
                    }
                }
            }
        }

        private  bool DimensionsInBounds(int x, int y)
        {
            return x >= 0 && x < Width && y >= 0 && y < Height;
        }
    }
}
