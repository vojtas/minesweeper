﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MineSweeper.Core
{
    public class Cell
    {
        public CellType Type { get; set; }
        public int MinesInArea { get; set; }

        public Cell(CellType type = CellType.Neutral)
        {
            Type = type;
            MinesInArea = 0;
        }

        public void AddMineInArea()
        {
            if (Type == CellType.Neutral)
                MinesInArea++;
        }
    }
}
