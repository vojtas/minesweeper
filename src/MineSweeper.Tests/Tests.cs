using FluentAssertions;
using MineSweeper.Core;
using NUnit.Framework;
using System;
using System.Diagnostics;

namespace MineSweeper.Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        private TimeSpan Measure(Action act) 
        {            
            var stopwatch = Stopwatch.StartNew();
            act();
            stopwatch.Stop();
            Console.WriteLine($"Took {stopwatch.Elapsed}");
            return stopwatch.Elapsed;
        }

        [Test]
        public void GenerateFieldCallResult_ShouldNotBeEmpty()
        {
            Measure(() => {
                var mineSeeper = new MineSweeper.Core.MineSweeper(20, 20, 100);
                mineSeeper.GenerateField();
                var s = mineSeeper.ToString();
                s.Should().NotBeEmpty();
                Console.WriteLine(s);
            });
        }

        [Test]
        public void GenerateField2CallResult_ShouldNotBeEmpty()
        {
            Measure(() => {
                var mineSeeper = new MineSweeper.Core.MineSweeper(20, 20, 100);
                mineSeeper.GenerateField();
                var s = mineSeeper.ToString();
                s.Should().NotBeEmpty();
                Console.WriteLine(s);
            });        
        }

        [Test]
        public void PassMatrix()
        {
            var mineSeeper = new MineSweeper.Core.MineSweeper(new bool[,] {{ true, false, false }, { false, true, false }, { false, false, false }});
            var s = mineSeeper.ToString();
            s.Should().Be("|X|2|1|\r\n|2|X|1|\r\n|1|1|1|\r\n");
        }

        [Test]
        public void PassMatrixDiag()
        {
            var mineSeeper = new MineSweeper.Core.MineSweeper(new bool[,] {
                { false, false, false, true, false, true, false, false, false, false, false, false, false, false, false }, 
                { false, false, false, false, true, false, false, false, false, false, false, false, false, true, false },
                { false, false, false, false, false, false, false, false, true, false, false, false, false, true, false },
                { false, false, true, false, false, false, true, false, false, false, false, false, false, true, false },
                { false, false, false, false, false, false, false, false, false, false, false, false, true, false, false },
                { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false },
                { false, false, false, false, true, true, false, true, true, false, false, true, false, false, true },
                { true, false, true, false, false, false, false, false, false, false, false, false, false, false, false },
                { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false },
                { false, false, true, false, false, false, false, false, false, false, false, true, false, false, false },
                { false, false, false, false, false, false, false, false, false, false, false, false, false, true, false },
                { true, false, false, false, false, false, false, false, false, false, true, false, false, false, false },
                { false, false, false, false, true, false, false, false, true, false, false, false, false, false, false },
                { false, false, false, false, true, true, false, false, false, false, true, false, false, false, true },
                { false, false, false, false, false, false, false, false, false, false, false, false, false, true, false }
                
                });
            var s = mineSeeper.ToString();
            s.Should().Be(@"| | |1|X|3|X|1| | | | | |1|1|1|
| | |1|2|X|2|1|1|1|1| | |2|X|2|
| |1|1|2|1|2|1|2|X|1| | |3|X|3|
| |1|X|1| |1|X|2|1|1| |1|3|X|2|
| |1|1|1| |1|1|1| | | |1|X|2|1|
| | | |1|2|2|2|2|2|1|1|2|2|2|1|
|1|2|1|2|X|X|2|X|X|1|1|X|1|1|X|
|X|2|X|2|2|2|2|2|2|1|1|1|1|1|1|
|1|3|2|2| | | | | | |1|1|1| | |
| |1|X|1| | | | | | |1|X|2|1|1|
|1|2|1|1| | | | | |1|2|2|2|X|1|
|X|1| |1|1|1| |1|1|2|X|1|1|1|1|
|1|1| |2|X|3|1|1|X|3|2|2| |1|1|
| | | |2|X|X|1|1|1|2|X|1|1|2|X|
| | | |1|2|2|1| | |1|1|1|1|X|2|
");
        }
    }
}